from datetime import date
from datetime import datetime
from datetime import timedelta
from flask import Flask, render_template
from pymongo import MongoClient
#import dateutil
import pymongo
conn = MongoClient()

app = Flask(__name__)

db = conn.Production
@app.route('/')
def load():
	return render_template("index.html")


@app.route('/productionOrders')
def display():
	collection = db.production_orders
	cursor = collection.find() 
	for record in cursor: 
   		print(record) 
	return "db.production_orders.find().pretty()"

@app.route('/after24')
def display24():
	collection = db.production_orders
	cursor = collection.find({'DATE' : { '$gt' : datetime(2020,9,20,0,0,0)}})
	for record in cursor:
		print(record)
	return "After 24th sept."

@app.route('/shiftRecords')
def shift():
	first = datetime(2020,9,23,0,0,0)
	last = datetime(2020,9,24,0,0,0)
	collection = db.shift_records
	cursor = collection.find({"SHIFT START TIME": {'$gte': first, '$lte': last}})
	for record in cursor:
		print(record)
	return "Shift start date"

if __name__ == "__main__":
	app.run(debug=True)
